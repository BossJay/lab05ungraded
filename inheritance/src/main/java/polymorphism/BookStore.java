package polymorphism;

public class BookStore {
    public static void main(String[] args) {
        Book[] books = new Book[5];
        books[0] = new Book("The Turtle", "John");
        books[1] = new ElectronicBook("The Animal King", "Michael", 50);
        books[2] = new Book("The Giraffe", "Austin");
        books[3] = new ElectronicBook("The Dog and Cat", "Harry", 20);
        books[4] = new ElectronicBook("The Rat", "Chris", 50);

        for (int i = 0; i < books.length; i++) {
            System.out.println(books[i]);
        }
    }
}
